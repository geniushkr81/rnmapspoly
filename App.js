import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React, { Component } from "react";
import { Provider, Appbar, Card } from 'react-native-paper';
import MapView, {Marker, Polyline} from 'react-native-maps';

export default function App() {
  const [coordinates] = React.useState([
    {
      latitude: 19.3706858,
      longitude: -99.1606515,
    },
    {
      latitude: 19.33933,
      longitude: -99.16561,
    },
    {
      latitude: 19.37097,
      longitude: -99.17556,
    },
    {
      latitude: 19.3706858,
      longitude: -99.1606515,
    },
  ]);
  const _goBack = () => console.log('Went back');
  const _handleSearch = () => console.log('Searching');
  const _handleMore = () => console.log('Shown more');

  return (
    <Provider>
      <Appbar.Header style={styles.header}>
        <Appbar.BackAction onPress={_goBack} />
        <Appbar.Content title="My App" subtitle="Subtitle" />
        <Appbar.Action icon="magnify" onPress={_handleSearch} />
        <Appbar.Action icon="dots-vertical" onPress={_handleMore} />
      </Appbar.Header>
      <View style={styles.mainbox}>
        <MapView
          style={styles.mapView}
          initialRegion={{
            latitude: 19.3706858,
            longitude: -99.1606515,
            latitudeDelta: 0.0322,
            longitudeDelta: 0.0321,
          }}>
            <Marker coordinate={coordinates[0]} />
            <Marker coordinate={coordinates[1]} />
            <Marker coordinate={coordinates[2]} />
            <Polyline
              coordinates={coordinates}
              strokeColor="#000"
              strokeColors={['#7F0000']}
              strokeWidth={3}
            />
          </MapView>
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  mainbox: {
    textAlign:'center',
    margin: 0,
    flex: 5,
    justifyContent: 'space-between',
  },
  mapView: {
    flex: 25,
  }
});
